import java.util.Random;

public class Runner {
    public static void main(String[] args) {
        Random random = new Random();
        Mouse mouseOne = new Mouse("Гадя", random.nextInt(100), random.nextInt(100));
        Mouse mouseTwo = new Mouse("Даздранагон", random.nextInt(100), random.nextInt(100));
        String[] colors = {"Зеленый", "Красный", "Синий", "Фиолетовый", "Терракотовый"};
        int colorNumber = 0;
        Bush[] bushArr = new Bush[18];
        for (int i = 0; i < bushArr.length; i++) {
            colorNumber = random.nextInt(5);
            bushArr[i] = new Bush();
            bushArr[i].setColor(colors[colorNumber]);
            bushArr[i].setX(random.nextInt(100));
            bushArr[i].setY(random.nextInt(100));
        }
        System.out.println(mouseOne.getName() + mouseOne.getX() + mouseOne.getY());
        System.out.println(mouseTwo.getName() + mouseTwo.getX() + mouseTwo.getY());
        int wind = 0;
        while (true) {
            mouseOne.runWithWind();
            mouseTwo.runWithWind();

            boolean mouseOneCoincidence = false, mouseTwoCoincidence = false;
            for (int i = 0; i < bushArr.length; i++) {
                if (bushArr[i].getX() == mouseOne.getX() && bushArr[i].getY() == mouseOne.getY()) {
                    mouseOneCoincidence = true;
                    mouseOne.setColor(bushArr[i].getColor());
                    System.out.println("Победила мышь " + mouseOne.getName() +
                            ". Она нашла куст в координатах " + bushArr[i].getX() + ":" + bushArr[i].getY() +
                            " и перекрасилась в цвет " + mouseOne.getColor());

                }
                if (bushArr[i].getX() == mouseTwo.getX() && bushArr[i].getY() == mouseTwo.getY()) {
                    mouseTwoCoincidence = true;
                    mouseTwo.setColor(bushArr[i].getColor());
                    System.out.println("Победила мышь " + mouseTwo.getName() +
                            ". Она нашла куст в координатах " + bushArr[i].getX() + ":" + bushArr[i].getY() +
                            " и перекрасилась в цвет " + mouseTwo.getColor());
                }
            }
            if (mouseOneCoincidence | mouseTwoCoincidence) {
                break;
            }
        }
    }
}
