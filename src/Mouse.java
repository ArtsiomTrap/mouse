import java.util.Random;

public class Mouse {
    private int x;
    private int y;
    private String name;
    private String color="white";

    public Mouse(String name, int x,int y){
        this.name=name;
        this.x=x;
        this.y=y;
    }
    public Mouse(){}

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public String getName() {
        return name;
    }
    public String getColor() {
        return color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    private boolean flagOfRun=false;
    public void runWithWind(){
        Random random=new Random();
        int windX=random.nextInt(3);
        int windY=random.nextInt(3);
        if (x+1+windX>99|y+1+windY>99){
            flagOfRun=false;
        }
        if (x-1-windX<0|y-1-windY<0){
            flagOfRun=true;
        }
        if(flagOfRun){
            x=x+1+windX;
            y=y+1+windY;
        }else {
            x=x-1-windX;
            y=y-1-windY;
        }
    }
}
