public class Bush {
    private int x;
    private int y;
    private String color;

    public Bush(){}

    public Bush(String color, int x, int y){
        this.color=color;
        this.x=x;
        this.y=y;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
